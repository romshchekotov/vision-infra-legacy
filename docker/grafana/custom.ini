##################### Grafana Configuration ##############################
# possible values : production, development
app_mode = production
instance_name = ${HOSTNAME}

#################################### Paths ###############################
[paths]
data = data
temp_data_lifetime = 24h
logs = data/log
plugins = data/plugins
provisioning = ${PROVISIONING}

#################################### Server ##############################
[server]
protocol = ${PROTOCOL}
http_addr =
http_port = 3000
domain = ${SUBDOMAIN}.${DOMAIN}
enforce_domain = false
root_url = %(protocol)s://%(domain)s/
serve_from_sub_path = false
router_logging = false
static_root_path = public
enable_gzip = false

# Unix socket path
socket = /tmp/grafana.sock
read_timeout = 0

#################################### Database ############################
[database]
type = sqlite3
host = 127.0.0.1:3306
name = grafana
user = root
password = ${ADMIN_PASSWORD}
max_idle_conn = 2
conn_max_lifetime = 14400
ssl_mode = disable
path = grafana.db
cache_mode = private

#################################### Analytics ###########################
[analytics]
reporting_enabled = false

#################################### Security ############################
[security]
admin_user = ${ADMIN_USER}
admin_password = ${ADMIN_PASSWORD}
secret_key = ${SECRET_KEY}
encryption_provider = secretKey.v1
available_encryption_providers =
disable_gravatar = false
data_source_proxy_whitelist =
disable_brute_force_login_protection = false
cookie_secure = false
cookie_samesite = lax
allow_embedding = false
strict_transport_security = false
strict_transport_security_max_age_seconds = 86400
strict_transport_security_preload = false
strict_transport_security_subdomains = false
x_content_type_options = true
x_xss_protection = true
content_security_policy = false
content_security_policy_template = """script-src 'self' 'unsafe-eval' 'unsafe-inline' 'strict-dynamic' $NONCE;object-src 'none';font-src 'self';style-src 'self' 'unsafe-inline' blob:;img-src * data:;base-uri 'self';connect-src 'self' grafana.com ws://$ROOT_PATH wss://$ROOT_PATH;manifest-src 'self';media-src 'none';form-action 'self';"""


#################################### Dashboards ##################

[dashboards]
versions_to_keep = 20
min_refresh_interval = 5s
# Path to the default home dashboard. If this value is empty, then Grafana uses StaticRootPath + "dashboards/home.json"
default_home_dashboard_path =

#################################### Users ###############################
[users]
allow_sign_up = false
allow_org_create = false
auto_assign_org = true
auto_assign_org_id = 1
auto_assign_org_role = Viewer
verify_email_enabled = false
login_hint = email or username
password_hint = password
default_theme = dark

# Path to a custom home page. Users are only redirected to this if the default home dashboard is used. It should match a frontend route and contain a leading slash.
home_page =

# External user management
external_manage_link_url =
external_manage_link_name =
external_manage_info =
viewers_can_edit = false
editors_can_admin = false
user_invite_max_lifetime_duration = 24h
hidden_users =

[auth]
login_cookie_name = grafana_session
login_maximum_inactive_lifetime_duration =
login_maximum_lifetime_duration =
token_rotation_interval_minutes = 10
disable_login_form = false
disable_signout_menu = false
signout_redirect_url =
oauth_auto_login = false
oauth_state_cookie_max_age = 600
oauth_skip_org_role_update_sync = false
api_key_max_seconds_to_live = -1
sigv4_auth_enabled = false
sigv4_verbose_logging = false

#################################### GitLab Auth #########################
[auth.gitlab]
enabled = ${GITLAB_ENABLED}
allow_sign_up = ${GITLAB_ENABLED}
client_id = ${GITLAB_CLIENT_ID}
client_secret = ${GITLAB_CLIENT_SECRET}
scopes = read_api
auth_url = https://gitlab.com/oauth/authorize
token_url = https://gitlab.com/oauth/token
api_url = https://gitlab.com/api/v4
allowed_groups = ${GITLAB_GROUP}

#################################### Logging ##########################
[log]
mode = console file
level = info

[log.console]
level = debug
format = console

[log.file]
level = info
format = text
log_rotate = true
max_lines = 1000000
max_size_shift = 28
daily_rotate = true
max_days = 7


#################################### Unified Alerting ####################
[unified_alerting]
enabled = true
admin_config_poll_interval = 60s
alertmanager_config_poll_interval = 60s
execute_alerts = true
evaluation_timeout = 30s
max_attempts = 3
min_interval = 10s


#################################### Explore #############################
[explore]
enabled = true

#################################### Query History #############################
[query_history]
enabled = true

#################################### Grafana.com integration  ##########################
[grafana_net]
url = https://grafana.com

[grafana_com]
url = https://grafana.com

[plugins]
enable_alpha = false
app_tls_skip_verify_insecure = false
plugin_admin_enabled = true
plugin_admin_external_manage_enabled = false
plugin_catalog_url = https://grafana.com/grafana/plugins/


[date_formats]
full_date = YYYY-MM-DD HH:mm:ss
interval_second = HH:mm:ss
interval_minute = HH:mm
interval_hour = MM/DD HH:mm
interval_day = MM/DD
interval_month = YYYY-MM
interval_year = YYYY
use_browser_locale = false
default_timezone = browser

[expressions]
enabled = true