terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "~> 2.13.0"
    }
  }
}

provider "docker" {
  host = (var.os == "win" ? "npipe:////.//pipe//docker_engine" : null)
}

locals {
  database_creds  = "${var.database_user}:${var.database_password}"
  database_domain = "${var.company_prefix}-postgres:5432"
  database_url    = "postgresql://${local.database_creds}@${local.database_domain}"
}

# Internal Docker Network
resource "docker_network" "internal" {
  name       = "${var.company_prefix}-internal"
  attachable = true
  driver     = "bridge"
}

# Discord Bot
resource "docker_image" "bot" {
  name = "${var.company_name}/bot"
  build {
    path = "bot/"
    tag  = ["${var.company_name}/bot:1.0.0"]
    label = {
      "author" = var.infrastructure_maintainer
    }
  }

  # Ensure that the dependent containers are
  # created before creating this bot image.
  depends_on = [
    docker_container.postgres,
    docker_container.loki
  ]
}

resource "docker_container" "bot" {
  image = docker_image.bot.latest
  name  = "${var.company_prefix}-bot"

  env = [
    "ENVIRONMENT=${var.environment}",
    "TOKEN=${var.discord_bot_token}",
    "CLIENT_ID=${var.discord_bot_client_id}",
    "DEV_GUILD=${var.discord_bot_dev_guild}",
    "BOT_HOST=${var.company_prefix}-bot",
    "GRAFANA_USER=${var.company_prefix}_grafana",
    "BOT_DATABASE=${local.database_url}/${var.company_prefix}_bot_db?schema=public",
    "INDEX_DATABASE=${local.database_url}/${var.company_prefix}_grafana_db?schema=public",
    "METRICS_PORT=${var.metrics_port}",
    "LOKI_URL=http://${var.company_prefix}-loki:3100",
  ]

  networks_advanced {
    name = docker_network.internal.name
  }

  mounts {
    type = "bind"
    source = "${abspath(path.root)}/bot/prisma"
    target = "/home/node/app/prisma"
  }

  restart = "unless-stopped"
}