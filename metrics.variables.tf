# Grafana
variable "grafana_admin_user" {
  type        = string
  description = "Grafana admin user"
  default     = "admin"
}

variable "grafana_admin_password" {
  type        = string
  description = "Grafana admin password"
  sensitive   = true
}

variable "grafana_secret_key" {
  type        = string
  description = "Grafana secret key"
  sensitive   = true
}

variable "grafana_subdomain" {
  type        = string
  description = "Grafana Subdomain"
  default     = "metrics"
}

# Prometheus
variable "metrics_port" {
  type        = string
  description = "Metrics port"
  default     = "9090"
}

# OAuth2
variable "gitlab_client_id" {
  type        = string
  description = "GitLab Client ID"
  sensitive   = true
  default     = "0000000000000000000000000000000000000000000000000000000000000000"

  validation {
    condition     = can(regex("^\\w{64}$", var.gitlab_client_id))
    error_message = "Invalid GitLab Client ID was specified!"
  }
}

variable "gitlab_client_secret" {
  type        = string
  description = "GitLab Client Secret"
  sensitive   = true
  default     = "0000000000000000000000000000000000000000000000000000000000000000"

  validation {
    condition     = can(regex("^\\w{64}$", var.gitlab_client_secret))
    error_message = "Invalid GitLab Client Secret was specified!"
  }
}

variable "gitlab_group" {
  type        = string
  description = "Allowed GitLab Group"
  default     = "company-gitlab"
}